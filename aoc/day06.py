lines = []
with open("day06.txt") as file:
    for line in file:
        lines.append(line.rstrip())

string = "".join(lines)

# part1
buff = ""
ctr = 0
while ctr < len(string):
    buff = string[ctr:ctr+4]
    if len(buff) == len(set(buff)):
        print(ctr+4)
        break
    ctr += 1

# part2
buff = ""
ctr = 0
while ctr < len(string):
    buff = string[ctr:ctr+14]
    if len(buff) == len(set(buff)):
        print(ctr+14)
        break
    ctr += 1
    