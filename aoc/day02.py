lines = []
with open("day02.txt") as file:
    for line in file:
        lines.append(line.rstrip())

# part1
win = {"A": "Y", "B": "Z", "C": "X"}
values = {"X": 1, "Y": 2, "Z": 3}
draw = {"A": "X", "B": "Y", "C": "Z"}

points = 0
for l in lines:
    points += values[l[2]]
    if win[l[0]] == l[2]:
        points += 6
    if draw[l[0]] == l[2]:
        points += 3
print(points)

# part2
round = {"X": 0, "Y": 3, "Z": 6}
Z = {"A": 2, "B": 3, "C": 1}
Y = {"A": 1, "B": 2, "C": 3}
X = {"A": 3, "B": 1, "C": 2}

points = 0
for l in lines:
    points += round[l[2]]
    if l[2] == "X":
        points += X[l[0]]
        # print(f"lose to {l[0]} with {round[l[2]]+X[l[0]]} points")
    if l[2] == "Y":
        points += Y[l[0]]
        # print(f"draw to {l[0]} with {round[l[2]]+Y[l[0]]} points")
    if l[2] == "Z":
        points += Z[l[0]]
        # print(f"win to {l[0]} with {round[l[2]]+Z[l[0]]} points")
print(points)
