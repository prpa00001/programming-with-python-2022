import re

lines = []
with open("day04.txt") as file:
    for line in file:
        lines.append(line.rstrip())

ctr1 = 0
ctr2 = 0
for l in lines:
    t = re.split(r",|-",l)
    a = set(range(int(t[0]), int(t[1])+1))
    b = set(range(int(t[2]), int(t[3])+1))
    # part1
    if a.issubset(b) or b.issubset(a):
        ctr1 += 1
    # part2
    if a&b:
        ctr2 += 1

print(ctr1)
print(ctr2)

