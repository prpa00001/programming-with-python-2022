lines = []
with open("day10.txt") as file:
    for line in file:
        lines.append(line.rstrip())

# part1
X = 1
cycle = [1]
signal = []
for l in lines:
    if "noop" in l:
        cycle.append(X)
    if "addx" in l:
        cycle.append(X)
        X += int(l.split(" ")[1])
        cycle.append(X)

cycles = [20, 60, 100, 140, 180, 220]
sum = 0
for c in cycles:
    sum += (c*cycle[c-1])

print(sum)

# part2
width = 40
crt = []
ctr = 0
for c in cycle:
    if abs(c - (ctr%width)) <= 1:
        crt.append("#")
    else:
        crt.append(".")
    ctr += 1

for i in range(0, len(cycle) // width):
    print(''.join(crt[i * width:(i + 1) * width]))
