lines = []
with open("day03.txt") as file:
    for line in file:
        lines.append(line.rstrip())

# part1
sum = 0
for l in lines:
    # print(l[:len(l)//2], l[len(l)//2:])
    a = set(l[:len(l)//2])
    b = set(l[len(l)//2:])
    c = next(iter(a&b))
    if c.islower():
        sum += ord(c) - 96
    if c.isupper():
        sum += ord(c) - 38

print(sum)

# part2
sum = 0
i = 0
for i in range(0, len(lines), 3):
    a = set(lines[i])
    b = set(lines[i+1])
    c = set(lines[i+2])
    d = next(iter(a&b&c))
    if d.islower():
        sum += ord(d) - 96
    if d.isupper():
        sum += ord(d) - 38

print(sum)
