import re

lines = []
with open("day05.txt") as file:
    for line in file:
        lines.append(line.rstrip())

def get_stacks(lines):
    stacks = [[] for _ in range(9)]
    for l in lines[::-1]:
        if l[0:4] != "move":
            stacks[0].append(l[0:3])
            stacks[1].append(l[4:7])
            stacks[2].append(l[8:11])
            stacks[3].append(l[12:15])
            stacks[4].append(l[16:19])
            stacks[5].append(l[20:23])
            stacks[6].append(l[24:27])
            stacks[7].append(l[28:31])
            stacks[8].append(l[32:35])

    for i in range(len(stacks)):
        stacks[i] = list(filter(None, [x.strip(' ') for x in stacks[i]]))
    return stacks

def pop_items(l, n):
    items = []
    while n:
        items.append((l.pop()))
        n -= 1
    return items

def push_items(l, items):
    for i in items:
        l.append(i)
    return l

def part1(stacks):
    for l in lines:
        if l[0:4] == "move":
            f = int(re.search('from (.*) to', l).group(1))-1
            s = int(re.search('to (.*)', l).group(1))-1
            n = int(re.search('move (.*) from', l).group(1))
            items = pop_items(stacks[f], n)
            stacks[s] = push_items(stacks[s], items)

    result = ""
    for s in stacks:
        result += s[len(s)-1].strip("[").strip("]")

    print(result)

def push_items_same_order(l, items):
    items = items[::-1]
    for i in items:
        l.append(i)
    return l

def part2(stacks):
    for l in lines:
        if l[0:4] == "move":
            f = int(re.search('from (.*) to', l).group(1))-1
            s = int(re.search('to (.*)', l).group(1))-1
            n = int(re.search('move (.*) from', l).group(1))
            items = pop_items(stacks[f], n)
            stacks[s] = push_items_same_order(stacks[s], items)

    result = ""
    for s in stacks:
        result += s[len(s)-1].strip("[").strip("]")

    print(result)

stack1 = get_stacks(lines)
part1(stack1)
stack2 = get_stacks(lines)
part2(stack2)
